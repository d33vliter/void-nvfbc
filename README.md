# void-nvfbc

**Script para instalar plugin nvfbc de OBS en VoidLinux**


## Pasos: 

`git clone --depth 1 https://gitlab.com/d33vliter/void-nvfbc`

`cd void-nvfbc`

`./installer.sh`

les pedira contraseña y listo!. (**NO EJECUTAR CON SUDO**)


## Enlaces:

**Nvidia Patch**: https://github.com/keylase/nvidia-patch

**OBS-Nvfbc**: https://gitlab.com/fzwoch/obs-nvfbc
