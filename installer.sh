#!/bin/sh

sudo xbps-install git base-devel libglvnd-devel meson obs obs-devel 

cd ~

git clone --depth 1 https://github.com/keylase/nvidia-patch

cd nvidia-patch

sudo ./patch-fbc.sh

cd ~

git clone --depth 1 https://gitlab.com/fzwoch/obs-nvfbc

cd obs-nvfbc

meson build

ninja -C build

mkdir -p ~/.config/obs-studio/plugins/nvfbc/bin/64bit

cp -vr build/nvfbc.so ~/.config/obs-studio/plugins/nvfbc/bin/64bit/

cd ~ && rm -rf nvidia-patch obs-nvfbc 

echo "Instalación finalizada!"
sleep 2s
clear
